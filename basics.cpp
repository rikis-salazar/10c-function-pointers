#include <iostream>
#include <vector>
#include <functional>

using std::cout;
using std::vector;
using std::ostream;


int my_function(){ return 2016; }

int square_element( int x ) { return x*x; } 
int double_element( int x ) { return 2*x; }

// operator<< 
ostream& operator<<( ostream&, int (*)() );
ostream& operator<<( ostream&, const vector<int>& );

// pointer to function as parameter
void modify_elements_in_vector( vector<int>& , int (*)(int) );


/** 
    modify_elements_in_vector is such a long name!!!

    Idea: Let us use a function pointer to shorten it!
 */
void (*meiv)( vector<int>&, int (*)(int) ) = modify_elements_in_vector;

/** 
    Problem: IT IS SUPER UGLY!!!
 */

// Soultion (1st try): typedef
typedef void (*newAndShortName_fp)( vector<int>&, int (*)(int) ) ;
newAndShortName_fp meiv2 = modify_elements_in_vector;

// Soultion (2nd try): aliases
using newerAndShorterName_fp = void (*)( vector<int>&, int (*)(int) );
newerAndShorterName_fp meiv3 = meiv2;

// Soultion (3rd try): std::function  (requieres <functional>)
std::function<void( vector<int>&, int (*)(int) )> meiv4 = meiv3;




int main(){

   cout << "Year: " 
        << my_function << "\n"; // Oops. I meant my_function()

   int (*ptr1_to_my_function)();
   int (*ptr2_to_my_function)(void);


   // Obtaining starting memory adress of my_function
   ptr1_to_my_function = my_function;  // <-- No parenthesis!
   ptr2_to_my_function = &my_function; // <-- & not needed (implicit conv.)
   int (*const ptr3_to_my_function)() = my_function;


   // Finally, let us call our function via our pointers
   cout << "Year: " << (*ptr1_to_my_function)() << "\n"; 
   cout << "Year: " << ptr2_to_my_function() << "\n";  // implicit deref.

   cout << "Address???: " << ptr1_to_my_function << "\n"; 
   cout << "Address!!!: " << reinterpret_cast<void*>(ptr1_to_my_function) << "\n"; 


   // Just for fun, let us swap ptr1... and ptr2...
   int (* dummy)() = ptr1_to_my_function;
   ptr1_to_my_function = ptr2_to_my_function;
   ptr2_to_my_function = dummy;

   // and since we already have dummy, we could try
   // to swap ptr2... and ptr3...
   dummy = ptr2_to_my_function;
   ptr2_to_my_function = ptr3_to_my_function;
   // ptr3_to_my_function = dummy; // <-- Oops!


   // Let us try again...
   cout << "\nAddress: " << my_function << "\n"; 
   cout << "Address: " << ptr1_to_my_function << "\n"; 
   cout << "Address: " << ptr2_to_my_function << "\n"; 
   cout << "Address: " << ptr3_to_my_function << "\n"; 


   // Let us transform a vector
   vector<int> a;
   for ( int i = 0 ; i < 5 ; i++ )
      a.push_back(i+1);
   cout << a << "\n";

   cout << "Squaring ...\n";
   //modify_elements_in_vector(a, square_element );
   meiv(a, square_element );
   cout << a << "\n";
   cout << "Doubling ...\n";
   //modify_elements_in_vector(a, double_element );
   //meiv2(a, double_element );
   //meiv3(a, double_element );
   meiv4(a, double_element );
   cout << a << "\n";


   return 0;
}



// Function definitions
ostream& operator<<( ostream& out, int (*f)() ){
   out << reinterpret_cast<void*>(f);
   return out;
}

ostream& operator<<( ostream& out, const vector<int>& v){
   for ( const int& x : v )
      out << x << " ";

   return out;
}

void modify_elements_in_vector( vector<int>& v, int (*some_function)(int) ){
   for ( int& x : v ){
      x = some_function(x);
   }

   return;
}
