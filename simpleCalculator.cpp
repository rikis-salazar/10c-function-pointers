#include <iostream>

double addition(int a, int b) { return static_cast<double>(a+b);}
double multiplication(int a, int b) { return static_cast<double>(a*b);}

typedef double (*arithFun_t)(int,int);

double perform_operation( int a, int b, arithFun_t f ){
   return f(a,b);
}

int main(){
   int x = 3;
   int y = 5;

   std::cout << "x = " << x << ", y = " << y << "\n";
   std::cout << "Choice?\n\t1: x + y\n\t2: x * y\n";

   int choice = 2;
   std::cout << "Result: " ;
   
   switch( choice ){
      case 1:
          std::cout << perform_operation(x,y,addition) << "\n";
	  break;
      case 2: 
          std::cout << perform_operation(x,y,multiplication) << "\n";
	  break;
   }

   return 0;
}

