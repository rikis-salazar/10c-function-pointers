## Pointers to functions and callbacks

In this repository we ..

 * discuss the basics behind pointers to functions
 * use function pointers to create callbacks
 * introduce equivalent (cleaner) notation for function pointers
 * implement a simple calculator that uses a callbacks to 
   perform computations.
